<?php

return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => '',
                    'dbname'   => 'db_quiz',
                )
            )
        ),
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'MyUser\Entity\MyUser',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => function ($user, $passwordGiven) {
                    return $user->getPassword() === md5(trim($passwordGiven));
                }
            ],
        ]
    ),
);
