<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Whoops\\Module' => $vendorDir . '/filp/whoops/src/deprecated/Zend/Module.php',
    'Whoops\\Provider\\Zend\\ExceptionStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/ExceptionStrategy.php',
    'Whoops\\Provider\\Zend\\RouteNotFoundStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/RouteNotFoundStrategy.php',
    'ZendDeveloperTools\\Module' => $vendorDir . '/zendframework/zend-developer-tools/Module.php',
    'Zf2Whoops\\Module' => $vendorDir . '/ghislainf/zf2-whoops/Module.php',
    'ZfcBase\\Module' => $vendorDir . '/zf-commons/zfc-base/Module.php',
    'ZfcUserDoctrineORM\\Entity\\User' => $vendorDir . '/zf-commons/zfc-user-doctrine-orm/src/ZfcUserDoctrineORM/Entity/User.php',
    'ZfcUserDoctrineORM\\Mapper\\User' => $vendorDir . '/zf-commons/zfc-user-doctrine-orm/src/ZfcUserDoctrineORM/Mapper/User.php',
    'ZfcUserDoctrineORM\\Module' => $vendorDir . '/zf-commons/zfc-user-doctrine-orm/Module.php',
    'ZfcUserDoctrineORM\\Options\\ModuleOptions' => $vendorDir . '/zf-commons/zfc-user-doctrine-orm/src/ZfcUserDoctrineORM/Options/ModuleOptions.php',
    'ZfcUser\\Module' => $vendorDir . '/zf-commons/zfc-user/Module.php',
);
