<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'myuser_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/MyUser/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'MyUser\Entity' => 'myuser_entity',
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'MyUser\Controller\User' => 'MyUser\Controller\UserController',
        ),
    ),

    'view_helpers' => array(
        'invokables' => array(
            'showMessages' => 'QuizMod\View\Helper\ShowMessages',
        ),
    ),

    'router' => array(
        'routes' => array(
            'user' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MyUser\Controller\User',
                        'action'     => 'index',
                    ),
                ),
            ),

            'user:login' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/login[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'MyUser\Controller\User',
                        'action'     => 'login',
                    ),
                ),
            ),

            'user:reg' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/reg[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'MyUser\Controller\User',
                        'action'     => 'reg',
                    ),
                ),
            ),

            'user:logout' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/logout[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'MyUser\Controller\User',
                        'action'     => 'logout',
                    ),
                ),
            ),

        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'factories' => array(
            'Zend\Authentication\AuthenticationService' => function ($serviceManager) {
                return $serviceManager->get('doctrine.authenticationservice.orm_default');
            },
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
