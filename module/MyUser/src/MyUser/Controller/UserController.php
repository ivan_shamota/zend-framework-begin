<?php

namespace MyUser\Controller;

use MyUser\Entity\MyUser;
use MyUser\Filter\FilterLog;
use MyUser\Filter\FilterReg;
use Zend\Mvc\Controller\AbstractActionController;
//use Back\Entity\User;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;


class UserController extends ActionController
{

    public function indexAction()
    {
        $users = $this->getEntityManager()
            ->getRepository(MyUser::class)
            ->findAll();
        $view = new ViewModel(array(
            'users' => $users,
        ));
        return $view;
    }

    public function loginAction()
    {
        if($this->identity())
        {
            $message = 'You have been signed up already';
            $this->flashMessenger()->addMessage($message);
            return $this->redirect()->toRoute('quiz');
        }
        $request = $this->getRequest();
        $filter = new FilterLog();
        $filter->setData($request->getPost());
        $message = '';
        /** @var AuthenticationService $authService */
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $adapter = $authService->getAdapter();
        if($request->isPost())
        {
            if($filter->isValid())
            {
                $email = htmlspecialchars($request->getPost('email'));
                $password = htmlspecialchars($request->getPost('password'));
                $adapter->setIdentityValue($email);
                $adapter->setCredentialValue($password);

                /**@var $authResult \Zend\Authentication\Result */
                $authResult = $authService->authenticate();
                if($authResult->isValid())
                {
                    $this->flashMessenger()->addMessage($message);
                    return $this->redirect()->toRoute('quiz');
                }
                else
                {
                    $message = 'Incorrect login or password!';
                }
            }
            else
            {
                $message = 'Error: you\'ve empty fields!';
            }
        }
        return new ViewModel(array('message' => $message, 'filter' => $filter));
    }

    public function regAction()
    {
        $filter = new FilterReg();
        /* @var $em EntityManager*/
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $user = new MyUser();
        $request = $this->getRequest();
        $filter->setData($request->getPost());
        if($request->isPost()) {
            if($filter->isValid()) {

                $user->setEmail(htmlspecialchars($request->getPost('email')));
                $user->setPassword(md5($request->getPost('password')));
                $em->persist($user);
                $em->flush();
                return $this->redirect()->toRoute('quiz');
            } else {
                $message = 'Error: you\'ve empty fields!';
                return new ViewModel(array('message' => $message, 'filter' => $filter));
            }
        }
        return new ViewModel([
            'filter' => $filter
        ]);
    }

    public function logoutAction()
    {
        /** @var AuthenticationService $authService */
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $authService->ClearIdentity();
        return $this->redirect()->toRoute('quiz');
    }

}