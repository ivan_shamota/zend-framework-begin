<?php
namespace MyUser\Filter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\Input;
use Zend\Validator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;


class FilterLog extends InputFilter
{
    public function __construct()
    {
        $email= new Input('email');
        $email->setRequired(true);
        $email
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new  StringLength(
                [
                    'min' => '1',
                    'message' => 'should not empty',
                ]
            ));

        $password= new Input('password');
        $password->setRequired(true);
        $password
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StringLength(
                [
                    'min' => 1,
                    'message' => 'should not empty',
                ]
            ));
        $this
            ->add($email)
            ->add($password);
    }
}
