<?php
namespace MyUser\Entity;
use Doctrine\ORM\Mapping as ORM;
use QuizMod\Entity\Answer;
use QuizMod\Entity\QuizPost;

/**
 * Class MyUser
 * @ORM\Entity()
 * @ORM\Table(name="my_user")
 * @package MyUser\Entity
 */
class MyUser
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="QuizMod\Entity\Answer", mappedBy="user")
     * @var Answer[]
     */
    protected $answers;

    /**
     * @ORM\OneToMany(targetEntity="QuizMod\Entity\QuizPost", mappedBy="user")
     * @var QuizPost[]
     */
    protected $questions;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;
    /**
     * @var integer
     *
     * @ORM\Column(name="is_active", type="smallint", nullable=false)
     */
    private $isActive = '1';

    /**
     * Set email
     *
     * @param string $email
     * @return MyUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set password
     *
     * @param string $password
     * @return MyUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return MyUser
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }
    /**
     * Get isActive
     *
     * @return integer
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return \QuizMod\Entity\Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param \QuizMod\Entity\Answer[] $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @return \QuizMod\Entity\QuizPost[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param \QuizMod\Entity\QuizPost[] $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}