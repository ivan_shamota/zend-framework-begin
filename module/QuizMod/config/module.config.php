<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'quiz_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/QuizMod/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'QuizMod\Entity' => 'quiz_entity',
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'QuizMod\Controller\QuizPost' => 'QuizMod\Controller\QuizController',
        ),
    ),

    'view_helpers' => array(
        'invokables' => array(
            'showMessages' => 'QuizMod\View\Helper\ShowMessages',
        ),
    ),

    'router' => array(
        'routes' => array(
            'quiz' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'index',
                    ),
                ),
            ),
            'quiz:view' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz/view/:id[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'view',
                    ),
                ),
            ),
            'quiz:remove' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz/remove/:id[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'remove',
                    ),
                ),
            ),

            'quiz:edit' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/quiz/edit/:id[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'edit',
                    ),
                ),
            ),

            'quiz:add' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz/add[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'add',
                    ),
                ),
            ),

            'quiz:addcomment' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz/:id/addcomment[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'addcomment',
                    ),
                ),
            ),

            'quiz:deletecomment' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quiz/:qid/deletecomment/:ansid[/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'QuizMod\Controller\QuizPost',
                        'action'     => 'deletecomment',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
                __DIR__ . '/../view',
        )
    )
);


