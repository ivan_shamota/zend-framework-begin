<?php

namespace QuizMod\Entity;

use Doctrine\ORM\Mapping as ORM;
use MyUser\Entity\MyUser;

/**
     * Class Answer
     * @ORM\Entity()
     * @ORM\Table(name="answer")
     * @ORM\HasLifecycleCallbacks
     * @package QuizMod\Entity
     */
class Answer{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text", length=255, nullable=false)
     */
    protected $text;

    /**
     * @var MyUser
     * @ORM\ManyToOne(targetEntity="MyUser\Entity\MyUser", inversedBy="answers")
     */
    protected $user;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="QuizPost", inversedBy="answers")
     *
     */
    protected $quiz;

    /**
     * @var string
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * Get id.
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id.
     *
     * @param int $id
     *
     * @return void
     */

    public function setId($id)
    {
        $this->id = (int)$id;
    }

     /**
     * Get text.
     *
     * @return string
     */

    public function getText()
    {
        return $this->text;
    }
    /**
     * Set text.
     *
     * @param string $text
     *
     * @return void
     */

    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get created.
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created.
     *
     * @param string $created
     *
     * @return void
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Set quizId.
     *
     * @param int $quizId
     *
     * @return void
     */
    public function setQuizId($quizId) {
        $this->quizId = $quizId;
    }

    /**
     *  Get quizId.
     *
     * @return int
     */
    public function getQuizId() {
        return $this->quizId;
    }

     /**
     * Helper function.
     */

    public function exchangeArray($data)
    {
        foreach ($data as $key => $val) {
            if (property_exists($this, $key)) {
                $this->$key = ($val !== null) ? $val : null;
            }
        }
    }

    /**
     * Helper function
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
        }
    }

    /**
     * @return MyUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param MyUser $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return QuizPost
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * @param QuizPost $quiz
     */
    public function setQuiz($quiz)
    {
        $this->quiz = $quiz;
    }
}
