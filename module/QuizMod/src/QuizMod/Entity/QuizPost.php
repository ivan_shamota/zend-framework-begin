<?php

namespace QuizMod\Entity;

use Doctrine\ORM\Mapping as ORM;
use MyUser\Entity\MyUser;

/**
     * Class QuizPost
     * @ORM\Entity()
     * @ORM\Table(name="quiz")
     * @ORM\HasLifecycleCallbacks
     * @package QuizMod\Entity
     */
class QuizPost{

    const UNPUBLISHED_STATUS = 0;
    const PUBLISHED_STATUS = 1;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="text", length=255, nullable=false)
     */
    protected $text;

    /**
     * @var MyUser
     * @ORM\ManyToOne(targetEntity="MyUser\Entity\MyUser", inversedBy="questions")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\OneToMany(targetEntity="QuizMod\Entity\Answer", mappedBy="quiz")
     * @var Answer[]
     */
    protected $answers;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */

    protected $state = self::PUBLISHED_STATUS;
    /**
     * Get id.
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id.
     *
     * @param int $id
     *
     * @return void
     */

    public function setId($id)
    {
        $this->id = (int)$id;
    }
    /**
     * Get title.
     *
     * @return string
     */

    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Set title.
     *
     * @param string $title
     *
     * @return void
     */

    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * Get text.
     *
     * @return string
     */

    public function getText()
    {
        return $this->text;
    }
    /**
     * Set text.
     *
     * @param string $text
     *
     * @return void
     */

    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get created.
     *
     * @return string
     */

    public function getCreated()
    {
        return $this->created;
    }
    /**
     * Set created.
     *
     * @param string $created
     *
     * @return void
     */

    public function setCreated($created)
    {
        $this->created = $created;
    }
    /**
     * Get state.
     *
     * @return int
     */

    public function getState()
    {
        return $this->state;
    }
    /**
     * Set state.
     *
     * @param int $state
     *
     * @return void
     */

    public function setState($state)
    {
        $this->state = $state;
    }
    /**
     * Helper function.
     */

    public function exchangeArray($data)
    {
        foreach ($data as $key => $val) {
            if (property_exists($this, $key)) {
                $this->$key = ($val !== null) ? $val : null;
            }
        }
    }

    /**
     * Helper function
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
        }
    }

    /**
     * @return MyUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param MyUser $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param Answer[] $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }
}