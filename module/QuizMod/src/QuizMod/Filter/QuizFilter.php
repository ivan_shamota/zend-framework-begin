<?php
namespace QuizMod\Filter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\Input;
use Zend\Validator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class QuizFilter extends InputFilter{
    public function __construct(){
        $title = new Input('title');
        $title->setRequired(true);
        $title
            ->getFilterChain()
            ->attach(new StringTrim());
        $title
            ->getValidatorChain()
            ->attach(new StringLength([
                'min' => 1,
            ]));

        $text = new Input('text');
        $text->setRequired(true);
        $text
            ->getFilterChain()
            ->attach(new StringTrim());
        $text
            ->getValidatorChain()
            ->attach(new StringLength([
                'min' => 1,
            ]));
        $state = new Input('state');
        $state->setRequired(false);

        $this
            ->add($title)
            ->add($text)
            ->add($state);
    }
}