<?php
namespace QuizMod\Filter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\Input;
use Zend\Validator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class CommentFilter extends InputFilter{
    public function __construct(){
        $text = new Input('text');
        $text->setRequired(true);
        $text
            ->getFilterChain()
            ->attach(new StringTrim());
        $text
            ->getValidatorChain()
            ->attach(new StringLength([
                'min' => 1,
            ]));

        $this
            ->add($text);
    }
}