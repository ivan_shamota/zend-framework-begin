<?php

namespace QuizMod\Controller;


use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;

class ActionController extends AbstractActionController{

    /**
     * @return EntityManager
     */
    public function getEntityManager(){
        return $this->getServiceLocator()->get('em');
    }

}