<?php
namespace QuizMod\Controller;

use QuizMod\Filter\CommentFilter;
use Zend\View\Model\ViewModel;
use QuizMod\Entity\QuizPost;
use QuizMod\Entity\Answer;
use QuizMod\Filter\QuizFilter;


class QuizController extends ActionController
{
    public function indexAction()
    {
        if($this->identity()){
            $quizes = $this->getEntityManager()
                ->getRepository(QuizPost::class)
                ->findBy(array(), array('created' => 'DESC'));
            $view = new ViewModel(array(
                'quizes' => $quizes,
            ));
        }
        else {
            $quizes = $this->getEntityManager()
                ->getRepository(QuizPost::class)
                ->findBy(array('state' => 1), array ('created' => 'DESC'));
            $view = new ViewModel(array(
                'quizes' => $quizes,
            ));
        }
    return $view;
    }

    public function addAction()
    {
        if (!$this->identity()){
            $message = "You don't have access to this page!";
            $this->flashMessenger()->addErrorMessage($message);
        } else {
            $request = $this->getRequest();
            $filter = new QuizFilter();
            $filter->setData($request->getPost()->toArray());
            if ($request->isPost()) {
                if ($filter->isValid()) {
                    $q = new QuizPost();
                    $q->setText($filter->getValue('text'));
                    $q->setTitle($filter->getValue('title'));
                    $q->setUser($this->identity());
                    if (NULL !== ($filter->getValue('state'))){
                        $q->setState(QuizPost::PUBLISHED_STATUS);
                    } else {
                        $q->setState(QuizPost::UNPUBLISHED_STATUS);
                    }
                    $this->getEntityManager()->persist($q);
                    $this->getEntityManager()->flush();
                    $message = 'Quiz succesfully saved!';
                    $this->flashMessenger()->addMessage($message);

                    // Redirect to list of blogposts
                    return $this->redirect()->toRoute('quiz');
                } else {
                    $message = 'Error while saving quiz';
                    $this->flashMessenger()->addErrorMessage($message);
                }
                return array('form' => $filter);
            }
        }
    }

    public function viewAction()
    {
        // Check if id and quiz exists.
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Quiz id doesn\'t set');
            return $this->redirect()->toRoute('quiz');
        }
        /** @var QuizPost $quiz */
        $quiz = $this->getEntityManager()
            ->getRepository(QuizPost::class)
            ->find($id);
        if (!$quiz) {
            $this->flashMessenger()->addErrorMessage(sprintf('Quiz with id %s doesn\'t exists', $id));
            return $this->redirect()->toRoute('quiz');
        }
            $answer = $quiz->getAnswers();
            $view = new ViewModel([
                'quiz' => $quiz,
                'answer' => $answer,
            ]);
        return $view;
    }

    public function editAction()
    {
        if (!$this->identity()){
            $message = "You don't have access to this page!";
            $this->flashMessenger()->addErrorMessage($message);
        } else {
            $request = $this->getRequest();
            $filter = new QuizFilter();
            $id = (int)$this->params()->fromRoute('id', 0);
            /** @var QuizPost $quiz */
            $quiz = $this->getEntityManager()
                ->getRepository(QuizPost::class)
                ->find($id);
            if ($quiz) {
                $filter->setData($quiz->getArrayCopy());
            } else {
                return $this->notFoundAction();
            }

            if ($request->isPost()) {
                $filter->setData($request->getPost()->toArray());
                if ($filter->isValid()) {
                    $q = $quiz;
                    $q->setText($filter->getValue('text'));
                    $q->setTitle($filter->getValue('title'));
                    $q->setUser($quiz->getUser());
                    if (NULL !== ($filter->getValue('state'))) {
                        $q->setState(QuizPost::PUBLISHED_STATUS);
                    } else {
                        $q->setState(QuizPost::UNPUBLISHED_STATUS);
                    }
                    $this->getEntityManager()->persist($q);
                    $this->getEntityManager()->flush();
                    $message = 'Quiz succesfully saved!';
                    return $this->redirect()->toRoute('quiz');
                }
            }
            $view = new ViewModel(array(
                'quiz' => $quiz,
                'filter' => $filter
            ));
            return $view;
        }
    }

    public function removeAction ()
    {
        if (!$this->identity()){
            $message = "You don't have access to this action!";
            $this->flashMessenger()->addErrorMessage($message);
            return $this->redirect()->toRoute('quiz');
        } else {
            $id = $this->params()->fromRoute('id', 0);
            $quiz = $this->getEntityManager()->getRepository(QuizPost::class)->find($id);
            if ($quiz) {
                $this->getEntityManager()->remove($quiz);
                $this->getEntityManager()->flush();
                $this->flashMessenger()->addErrorMessage('Quiz was successfully deleted');
            } else {
                $this->flashMessenger()->addErrorMessage('Quiz doesn\'t exists!');
            }
            return $this->redirect()->toRoute('quiz');
        }
    }

    public function addcommentAction()
    {
        if (!$this->identity()){
            $message = "You don't have access to this page!";
            $this->flashMessenger()->addErrorMessage($message);
        } else {
            $request = $this->getRequest();
            $filter = new CommentFilter();
            $filter->setData($request->getPost()->toArray());
            $quizId = (int)$this->params()->fromRoute('id', 0);
            $quiz = $this->getEntityManager()->getRepository(QuizPost::class)->find($quizId);
            if (!$quiz) {
                $message = 'This quiz doen\'t exist!';
                $this->flashMessenger()->addErrorMessage($message);
            }
            $ans = new Answer();
            if ($request->isPost()) {
                if ($filter->isValid()) {
                    $ans->setText($filter->getValue('text'));
                    $ans->setQuiz($quiz);
                    $ans->setUser($this->identity());
                    $this->getEntityManager()->persist($ans);
                    $this->getEntityManager()->flush();
                    $message = 'Comment succesfully added!';
                    $this->flashMessenger()->addMessage($message);

                    // Redirect to list of quizes
                    return $this->redirect()->toRoute('quiz:view', ["id" => $quizId]);
                    $message = 'Error while saving comment';
                    $this->flashMessenger()->addErrorMessage($message);
                }
            }
            $view = new ViewModel(array(
                'quiz' => $quiz,
                'answer' => $ans,
            ));
            return $view;
        }
    }

    public function deletecommentAction()
    {
        $qid = $this->params()->fromRoute('qid', 0);
        if (!$this->identity()) {
            $message = "You don't have access to this action!";
            $this->flashMessenger()->addErrorMessage($message);
            return $this->redirect()->toRoute('quiz:view', ["id" => $qid]);
        } else {
            $ansid = $this->params()->fromRoute('ansid', 0);
            $ans = $this->getEntityManager()->getRepository(Answer::class)->find($ansid);
            if ($ans) {
                $this->getEntityManager()->remove($ans);
                $this->getEntityManager()->flush();
                $this->flashMessenger()->addErrorMessage('Answer was successfully deleted');
            } else {
                $this->flashMessenger()->addErrorMessage('Answer does not exists!');
            }
            return $this->redirect()->toRoute('quiz:view', ["id" => $qid]);
        }
    }
}